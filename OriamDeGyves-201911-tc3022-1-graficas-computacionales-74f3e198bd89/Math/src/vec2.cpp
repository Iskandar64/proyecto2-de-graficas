#include "vec2.h"
#include <iostream>
#include <math.h>

cgmath::vec2::vec2() : x(0.0f), y(0.0f)
{
}

cgmath::vec2::vec2(float x, float y) : x(x), y(y)
{
}

float & cgmath::vec2::operator[](int i)
{
	float * coordinate = &x;
	return coordinate[i];
}

const float & cgmath::vec2::operator[](int i) const
{
	const float * coordinate = &x;
	return coordinate[i];
}

cgmath::vec2& cgmath::vec2::operator*=(float s)
{
	x = x * s;
	y = y * s;
	return *this;
}

cgmath::vec2 & cgmath::vec2::operator/=(float s)
{
	x = x / s;
	y = y / s;
	return *this;
}

cgmath::vec2& cgmath::vec2::operator+=(const vec2 v)
{
	x = x + v[0];
	y = y + v[1];
	return *this;
}

cgmath::vec2& cgmath::vec2::operator-=(const vec2 v)
{
	x = x - v[0];
	y = y - v[1];
	return *this;
}

bool cgmath::vec2::operator==(const vec2 & v) const
{
	return (x == v[0]) && (y == v[1]);
}

float cgmath::vec2::magnitude() const
{
	return sqrt((x*x) + (y*y));
}

void cgmath::vec2::normalize()
{
	float mag = magnitude();
	x = x / mag;
	y = y / mag;
}

float cgmath::vec2::magnitude(const vec2 & v)
{
	return sqrt((v.x*v.x) + (v.y*v.y));
}

cgmath::vec2 cgmath::vec2::normalize(const vec2 & v)
{
	float mag = magnitude(v);
	float x = v.x / mag;
	float y = v.y / mag;
	return vec2(x, y);
}

float cgmath::vec2::dot(const vec2 & a, const vec2 & b)
{
	return ((a.x*b.x) + (a.y*b.y));
}


