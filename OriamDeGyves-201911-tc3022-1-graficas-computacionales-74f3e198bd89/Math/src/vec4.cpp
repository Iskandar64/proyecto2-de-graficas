#include "vec4.h"
#include <iostream>
#include <math.h>

cgmath::vec4::vec4() : x(0.0f), y(0.0f), z(0.0f), w(0.0f)
{
}

cgmath::vec4::vec4(float x, float y, float z, float w) : x(x), 
y(y), z(z), w(w)
{
}

float & cgmath::vec4::operator[](int i)
{
	float * coordinate = &x;
	return coordinate[i];;
}

const float & cgmath::vec4::operator[](int i) const
{
	const float * coordinate = &x;
	return coordinate[i];
}

cgmath::vec4& cgmath::vec4::operator*=(float s)
{
	x = x * s;
	y = y * s;
	z = z * s;
	w = w * s;
	return *this;
}

cgmath::vec4 & cgmath::vec4::operator/=(float s)
{
	x = x / s;
	y = y / s;
	z = z / s;
	w = w / s;
	return *this;
}

cgmath::vec4& cgmath::vec4::operator+=(const vec4 v)
{
	x = x + v[0];
	y = y + v[1];
	z = z + v[2];
	w = w + v[3];
	return *this;
}

cgmath::vec4& cgmath::vec4::operator-=(const vec4 v)
{
	x = x - v[0];
	y = y - v[1];
	z = z - v[2];
	w = w - v[3];
	return *this;
}

bool cgmath::vec4::operator==(const vec4 & v) const
{
	return (x == v[0]) && (y == v[1]) && (z == v[2]) && (w == v[3]);
}

