#include "vec3.h"
#include <iostream>
#include <math.h>

cgmath::vec3::vec3() : x(0.0f), y(0.0f), z(0.0f)
{
}

cgmath::vec3::vec3(float x, float y, float z) : x(x), y(y), z(z)
{
}

float & cgmath::vec3::operator[](int i)
{
	float * coordinate = &x;
	return coordinate[i];
}

const float & cgmath::vec3::operator[](int i) const
{
	const float * coordinate = &x;
	return coordinate[i];
}

cgmath::vec3& cgmath::vec3::operator*=(float s)
{
	x = x * s;
	y = y * s;
	z = z * s;
	return *this;
}

cgmath::vec3 & cgmath::vec3::operator/=(float s)
{
	x = x / s;
	y = y / s;
	z = z / s;
	return *this;
}

cgmath::vec3& cgmath::vec3::operator+=(const vec3 v)
{
	x = x + v[0];
	y = y + v[1];
	z = z + v[2];
	return *this;
}

cgmath::vec3& cgmath::vec3::operator-=(const vec3 v)
{
	x = x - v[0];
	y = y - v[1];
	z = z - v[2];
	return *this;
}

bool cgmath::vec3::operator==(const vec3 & v) const
{
	return (x == v[0]) && (y == v[1]) && (z == v[2]);
}

float cgmath::vec3::magnitude() const
{
	return sqrt((x*x) + (y*y) + (z*z));
}

void cgmath::vec3::normalize()
{
	float mag = magnitude();
	x = x / mag;
	y = y / mag;
	z = z / mag;
}

float cgmath::vec3::magnitude(const vec3 & v)
{
	return sqrt((v.x*v.x) + (v.y*v.y) + (v.z*v.z));
}

cgmath::vec3 cgmath::vec3::normalize(const vec3 & v)
{
	float mag = magnitude(v);
	float x = v.x / mag;
	float y = v.y / mag;
	float z = v.z / mag;
	return vec3(x, y, z);
}

float cgmath::vec3::dot(const vec3 & a, const vec3 & b)
{
	return ((a.x*b.x) + (a.y*b.y) + (a.z*b.z));
}

cgmath::vec3 cgmath::vec3::cross(const vec3 & a, const vec3 & b)
{
	float nX = a[1] * b[2] - a[2] * b[1];
	float nY = a[2] * b[0] - a[0] * b[2];
	float nZ = a[0] * b[1] - a[1] * b[0];
	return vec3(nX, nY, nZ);
}

