#include "mat4.h"
#include "vec4.h"
#include "mat3.h"
#include <iostream>

cgmath::mat4::mat4() : n{ 0 }
{
}

cgmath::mat4::mat4(float diagonal) : n{ {diagonal, 0, 0, 0}, {0, diagonal, 0, 0}, {0, 0, diagonal, 0}, {0, 0, 0, diagonal} }
{
}

cgmath::mat4::mat4(const vec4& a, const vec4& b, const vec4& c, const vec4& d) : n{ {a.x, a[1], a.z, a[3]}, {b[0], b.y, b[2], b.w}, {c.x, c[1], c.z, c[3]}, {d[0], d.y, d[2], d.w} }
{
}

cgmath::vec4 & cgmath::mat4::operator[](int column)
{
	vec4 * vv = reinterpret_cast<vec4*>(n[column]);
	return *vv;
}

const cgmath::vec4 & cgmath::mat4::operator[](int column) const
{
	const vec4 * vv = reinterpret_cast<const vec4*>(n[column]);
	return *vv;
}

bool cgmath::mat4::operator==(const mat4 & m) const
{
	return (n[0][0] == m[0][0]) && (n[0][1] == m[0][1]) && (n[0][2] == m[0][2]) &&
		(n[1][0] == m[1][0]) && (n[1][1] == m[1][1]) && (n[1][2] == m[1][2]) &&
		(n[2][0] == m[2][0]) && (n[2][1] == m[2][1]) && (n[2][2] == m[2][2]);
}


cgmath::mat4 cgmath::mat4::inverse(const mat4 & m)
{
	
		float m11 = 1 * cgmath::mat3::determinant(mat3({ {m[1][1], m[1][2], m[1][3]},
			{m[2][1], m[2][2], m[2][3]}, {m[3][1], m[3][2], m[3][3]} }));
		float m12 = -1 * cgmath::mat3::determinant(mat3({ {m[1][0], m[1][2], m[1][3]},
			{m[2][0], m[2][2], m[2][3]}, {m[3][0], m[3][2], m[3][3]} }));
		float m13 = 1 * cgmath::mat3::determinant(mat3({ {m[1][0], m[1][1], m[1][3]},
			{m[2][0], m[2][1], m[2][3]}, {m[3][0], m[3][1], m[3][3]} }));
		float m14 = -1 * cgmath::mat3::determinant(mat3({ {m[1][0], m[1][1], m[1][2]},
			{m[2][0], m[2][1], m[2][2]}, {m[3][0], m[3][1], m[3][2]} }));
		float m21 = -1 * cgmath::mat3::determinant(mat3({ {m[0][1], m[0][2], m[0][3]},
			{m[2][1], m[2][2], m[2][3]}, {m[3][1], m[3][2], m[3][3]} }));
		float m22 = 1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][2], m[0][3]},
			{m[2][0], m[2][2], m[2][3]}, {m[3][0], m[3][2], m[3][3]} }));
		float m23 = -1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][1], m[0][3]},
			{m[2][0], m[2][1], m[2][3]}, {m[3][0], m[3][1], m[3][3]} }));
		float m24 = 1 * cgmath::mat3::determinant(mat3({ { m[0][0], m[0][1], m[0][2]},
			{ m[2][0], m[2][1], m[2][2] }, { m[3][0], m[3][1], m[3][2]} }));
		float m31 = 1 * cgmath::mat3::determinant(mat3({ {m[0][1], m[0][2], m[0][3]},
			{m[1][1], m[1][2], m[1][3]}, {m[3][1], m[3][2], m[3][3]} }));
		float m32 = -1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][2], m[0][3]},
			{m[1][0], m[1][2], m[1][3]}, {m[3][0], m[3][2], m[3][3]} }));
		float m33 = 1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][1], m[0][3]},
			{m[1][0], m[1][1], m[1][3]}, {m[3][0], m[3][1], m[3][3]} }));
		float m34 = -1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][1], m[0][2]},
			{m[1][0], m[1][1], m[1][2]}, {m[3][0], m[3][1], m[3][2]} }));
		float m41 = -1 * cgmath::mat3::determinant(mat3({ {m[0][1], m[0][2], m[0][3]},
			{m[1][1], m[1][2], m[1][3]}, {m[2][1], m[2][2], m[2][3]} }));
		float m42 = 1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][2], m[0][3]},
			{m[1][0], m[1][2], m[1][3]}, {m[2][0], m[2][2], m[2][3]} }));
		float m43 = -1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][1], m[0][3]},
			{m[1][0], m[1][1], m[1][3]}, {m[2][0], m[2][1], m[2][3]} }));
		float m44 = 1 * cgmath::mat3::determinant(mat3({ {m[0][0], m[0][1], m[0][2]},
			{m[1][0], m[1][1], m[1][2]}, {m[2][0], m[2][1], m[2][2]} }));

		float A = 1 /float ((m[0][0] * m11) - (m[1][0] * m21 * -1) + 
			(m[2][0] * m31) - (m[3][0] * m41) * -1);

		return mat4({ {A * m11, A * m21, A * m31, A * m41},
			{A * m12, A * m22, A * m32, A * m42},
			{A * m13, A * m23, A * m33, A * m43}, 
			{A * m14, A * m24, A * m34, A * m44} });
}

