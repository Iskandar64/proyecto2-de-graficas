#include "mat3.h"
#include "vec3.h"
#include <iostream>

cgmath::mat3::mat3() : n{ 0 }
{	
}

cgmath::mat3::mat3(float diagonal) : n{ {diagonal, 0, 0}, {0, diagonal, 0}, {0, 0, diagonal} }
{
}

cgmath::mat3::mat3(const vec3& a, const vec3& b, const vec3& c) : n{{a.x, a[1], a.z}, {b[0], b.y, b[2]}, {c.x, c[1], c.z}}
{
}

cgmath::vec3 & cgmath::mat3::operator[](int column)
{
	vec3 * vv = reinterpret_cast<vec3*>(n[column]);
	return *vv;
}

const cgmath::vec3 & cgmath::mat3::operator[](int column) const
{
	const vec3 * vv = reinterpret_cast<const vec3*>(n[column]);
	return *vv;
}

bool cgmath::mat3::operator==(const mat3 & m) const
{
	return (n[0][0] == m[0][0]) && (n[0][1] == m[0][1]) && (n[0][2] == m[0][2]) && 
		(n[1][0] == m[1][0]) && (n[1][1] == m[1][1]) && (n[1][2] == m[1][2]) && 
		(n[2][0] == m[2][0]) && (n[2][1] == m[2][1]) && (n[2][2] == m[2][2]);
}

float cgmath::mat3::determinant(const mat3 & m)
{
	float ans = (m[0][0] * (m[1][1] * m[2][2] - m[2][1] * m[1][2]) 
		- m[1][0] * (m[0][1] * m[2][2] - m[2][1] * m[0][2])
		+ m[2][0] * (m[0][1] * m[1][2] - m[1][1] * m[0][2]));
	return ans;
}

cgmath::mat3 cgmath::mat3::inverse(const mat3 & m)
{
	float A = (m[1][1] * m[2][2]) - (m[2][1] * m[1][2]);
	float B = -((m[1][0] * m[2][2]) - (m[2][0] * m[1][2]));
	float C = (m[1][0] * m[2][1]) - (m[2][0] * m[1][1]);
	float D = -((m[0][1] * m[2][2]) - (m[2][1] * m[0][2]));
	float E = (m[0][0] * m[2][2]) - (m[2][0] * m[0][2]);
	float F = -((m[0][0] * m[2][1]) - (m[2][0] * m[0][1]));
	float G = (m[0][1] * m[1][2]) - (m[1][1] * m[0][2]);
	float H = -((m[0][0] * m[1][2]) - (m[1][0] * m[0][2]));
	float I = (m[0][0] * m[1][1]) - (m[1][0] * m[0][1]);
	float M = 1 / float(determinant(m));
	return mat3({{M * A, M * D, M * G}, {M * B, M *E, M * H},
		{M * C, M * F, M * I}});
}

cgmath::mat3 cgmath::mat3::transpose(const mat3 & m)
{
	return mat3(vec3(m[0][0], m[1][0], m[2][0]),
		vec3(m[0][1], m[1][1], m[2][1]),
		vec3(m[0][2], m[1][2], m[2][2]));
}
