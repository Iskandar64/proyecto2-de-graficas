#pragma once
#include <iostream>
#include "vec3.h"

namespace cgmath
{
	class mat3
	{
	public:
		float n[3][3];

		mat3();
		mat3(float diagonal);
		mat3(const vec3& a, const vec3& b, const vec3& c);

		vec3& operator[](int column);
		const vec3& operator[](int column) const;
		bool operator==(const mat3& m) const;
		static float determinant(const mat3& m);
		static mat3 inverse(const mat3& m);
		static mat3 transpose(const mat3& m);

		friend std::ostream& operator<<(std::ostream& os, const mat3& m)
		{
			os << m[0][0] << " " << m[1][0] << " " << m[2][0] << "\n"
				<< m[0][1] << " " << m[1][1] << " " << m[2][1] << "\n"
				<< m[0][2] << " " << m[1][2] << " " << m[2][2];
			return os;
		}
	};

	inline vec3 operator*(const mat3& m, const vec3& v)
	{
		float v1 = (m[0][0] * v.x + m[1][0] * v.y + m[2][0] * v.z);
		float v2 = (m[0][1] * v.x + m[1][1] * v.y + m[2][1] * v.z);
		float v3 = (m[0][2] * v.x + m[1][2] * v.y + m[2][2] * v.z);
		return vec3(v1, v2, v3);
	}

	inline mat3 operator*(const mat3& m1, const mat3& m2)
	{
		float A = (m1[0][0] * m2[0][0]) + (m1[1][0] * m2[0][1]) + (m1[2][0] * m2[0][2]);
		float B = (m1[0][0] * m2[1][0]) + (m1[1][0] * m2[1][1]) + (m1[2][0] * m2[1][2]);
		float C = (m1[0][0] * m2[2][0]) + (m1[1][0] * m2[2][1]) + (m1[2][0] * m2[2][2]);
		float D = (m1[0][1] * m2[0][0]) + (m1[1][1] * m2[0][1]) + (m1[2][1] * m2[0][2]);
		float E = (m1[0][1] * m2[1][0]) + (m1[1][1] * m2[1][1]) + (m1[2][1] * m2[1][2]);
		float F = (m1[0][1] * m2[2][0]) + (m1[1][1] * m2[2][1]) + (m1[2][1] * m2[2][2]);
		float G = (m1[0][2] * m2[0][0]) + (m1[1][2] * m2[0][1]) + (m1[2][2] * m2[0][2]);
		float H = (m1[0][2] * m2[1][0]) + (m1[1][2] * m2[1][1]) + (m1[2][2] * m2[1][2]);
		float I = (m1[0][2] * m2[2][0]) + (m1[1][2] * m2[2][1]) + (m1[2][2] * m2[2][2]);
		return mat3({ {A, D, G}, {B, E, H}, {C, F, I} });
	}

}
